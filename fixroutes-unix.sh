#!/bin/sh
GWIP=$(netstat -rn | grep 10.40.96 | awk -F ' ' '{print $2}' | head -n 1)
echo $GWIP
netstat -rn > ./vpnlog

while IFS=" " read -r net gw flag netif; do
    if [ $gw == $GWIP ]; then
	route -n delete "$net" "$gw"
    fi
done < ./vpnlog
route -n add 10.0.0.0 "$GWIP"
rm ./vpnlog
