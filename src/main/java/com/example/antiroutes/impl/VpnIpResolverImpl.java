package com.example.antiroutes.impl;

import com.example.antiroutes.api.VpnIpResolver;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Component;

import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.util.List;

@Component
@Slf4j
public class VpnIpResolverImpl implements VpnIpResolver {

    private static final int IPV4_LENGTH = 4;
    private static final int IPV6_LENGTH = 16;

    @SneakyThrows
    @Override
    public String resolveVpnIpAddress() {
        List<InetAddress> localAddresses = getLocalAddresses();

        log.info("===== Detecting VPN =====");
        return StreamEx.of(localAddresses)
                .map(InetAddress::getHostAddress)
                .filter(ia -> ia.startsWith(VPN_IP_SUBNET_PREFIX))
                .findFirst()
                .orElseThrow(() -> new IllegalStateException("VPN IP address not found. Are you connected?"));
    }

    @SneakyThrows
    static List<InetAddress> getLocalAddresses() {
        log.info("===== Network Interfaces =====");
        return StreamEx.of(NetworkInterface.networkInterfaces())
                .mapToEntry(NetworkInterface::getInterfaceAddresses)
                .filterValues(ia -> !ia.isEmpty())
                .peekKeys(in -> log.info("# {} : {}", in.getIndex(), in.getDisplayName()))
                .values()
                .flatCollection(i -> i)
                .map(InterfaceAddress::getAddress)
                .filter(a -> a.getAddress().length == IPV4_LENGTH)
                .peek(a -> log.info("  Assigned address: {}", a.getHostAddress()))
                .toList();
    }
}
