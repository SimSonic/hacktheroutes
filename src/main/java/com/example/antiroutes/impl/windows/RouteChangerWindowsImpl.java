package com.example.antiroutes.impl.windows;

import com.example.antiroutes.api.Route;
import com.example.antiroutes.api.RouteChanger;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import java.net.InetAddress;
import java.net.NetworkInterface;

@Slf4j
public class RouteChangerWindowsImpl implements RouteChanger {

    @Override
    public void createRoute(Route route) {
        String interfaceId = getRouteInterfaceId(route);
        String metric = String.valueOf(route.getMetric());

        var processResult = WindowsProcessStarter.execute("route",
                "ADD", route.getAddress(),
                "MASK", route.getMask(),
                route.getGateway(),
                "METRIC", metric,
                "IF", interfaceId);

        // Не смогли.
        if (processResult.getExitCode() != 0) {
            String errorMessage = String.format("Cannot create route. Exit code: %d, message: %s",
                    processResult.getExitCode(),
                    processResult.getOutput());
            throw new IllegalStateException(errorMessage);
        }

        log.info("Route has been successfully created: {}", route);
    }

    @Override
    public void killRoute(Route route) {
        var processResult = WindowsProcessStarter.execute("route",
                "DELETE", route.getAddress(),
                "MASK", route.getMask());

        // Не смогли.
        if (processResult.getExitCode() != 0) {
            String errorMessage = String.format("Cannot remove route. Exit code: %d, message: %s",
                    processResult.getExitCode(),
                    processResult.getOutput());
            throw new IllegalStateException(errorMessage);
        }

        log.info("Route has been successfully deleted: {}", route);
    }

    @SneakyThrows
    private static String getRouteInterfaceId(Route route) {
        return StreamEx.of(NetworkInterface.networkInterfaces())
                .mapToEntry(NetworkInterface::getIndex, NetworkInterface::getInetAddresses)
                .flatMapValues(StreamEx::of)
                .mapValues(InetAddress::getHostAddress)
                .filterValues(route.getNetworkInterface()::equals)
                .keys()
                .findFirst()
                .map(String::valueOf)
                .orElseThrow();
    }
}
