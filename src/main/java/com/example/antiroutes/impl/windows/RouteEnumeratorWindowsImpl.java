package com.example.antiroutes.impl.windows;

import com.example.antiroutes.api.Route;
import com.example.antiroutes.api.RouteEnumerator;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.compile;

@Slf4j
public class RouteEnumeratorWindowsImpl implements RouteEnumerator {

    /**
     * Для парсинга нужных нам строк.
     */
    private static final Pattern IPV4_ROUTE_OUTPUT_PATTERN = compile("\\s*([\\d.]+)\\s*([\\d.]+)\\s*([\\d.]+|[\\w\\-]+)\\s*([\\d.]+)\\s*([\\d.]+)");

    /**
     * От "d.d.d.d" до "ddd.ddd.ddd.ddd".
     */
    private static final Pattern IPV4_PATTERN = compile("\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}");

    @SneakyThrows
    @Override
    public List<Route> enumerateRoutes() {
        log.info("===== Listing current routes =====");
        var processResult = WindowsProcessStarter.execute("route", "PRINT");
        if (processResult.getExitCode() != 0) {
            throw new IllegalStateException("Cannot execute printing routes.");
        }

        String output = processResult.getOutput();
        log.info(">           address|            mask|         gateway|       interface|metric");
        log.info("> -----------------|----------------|----------------|----------------|------");
        return StreamEx.of(output)
                .flatMap(String::lines)
                .mapToEntry(IPV4_ROUTE_OUTPUT_PATTERN::matcher)
                .filterValues(Matcher::matches)
                .mapValues(RouteEnumeratorWindowsImpl::toRoute)
                .peekKeys(line -> log.info("> {}", line))
                .values()
                .toList();
    }

    private static Route toRoute(MatchResult matcher) {
        String address = matcher.group(1);
        String mask = matcher.group(2);
        String gateway = matcher.group(3);
        String networkInterface = matcher.group(4);
        int metric = Integer.parseInt(matcher.group(5));
        return new Route(
                address,
                mask,
                IPV4_PATTERN.matcher(gateway).matches()
                        ? gateway
                        : Route.NO_GATEWAY,
                networkInterface,
                metric);
    }
}
