package com.example.antiroutes.impl.windows;

import lombok.Builder;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.experimental.UtilityClass;

import java.io.InputStream;
import java.nio.charset.Charset;

@UtilityClass
public class WindowsProcessStarter {

    /**
     * У вас может быть другая, но в целом-то пофиг.
     */
    private final Charset COMMAND_LINE_CODE_PAGE = Charset.forName("cp866");

    @SneakyThrows
    public ProcessResult execute(String... command) {
        Process process = new ProcessBuilder(command)
                .start();
        try (InputStream inputStream = process.getInputStream()) {
            byte[] bytes = inputStream.readAllBytes();
            String output = new String(bytes, COMMAND_LINE_CODE_PAGE);

            int exitCode = process.waitFor();

            return ProcessResult.builder()
                    .exitCode(exitCode)
                    .output(output)
                    .build();
        }
    }

    @Value
    @Builder
    public static class ProcessResult {

        int exitCode;
        String output;
    }
}
