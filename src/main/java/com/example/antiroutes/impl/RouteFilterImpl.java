package com.example.antiroutes.impl;

import com.example.antiroutes.api.Route;
import com.example.antiroutes.api.RouteFilter;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;

import java.net.InetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;

@Slf4j
public class RouteFilterImpl implements RouteFilter {

    /**
     * Реальные IP в банке, которые нужно туда маршрутизировать.
     */
    private static final List<RouteExclusion> REAL_BANK_ROUTES = List.of(
            new RouteExclusion("10.0.0.0", "255.0.0.0")
    );

    private static final int CREATE_ROUTE_METRIC = 1000;

    @Override
    public RouteFilterResult splitRoutes(List<Route> existingRoutes, String vpnIpAddress) {
        Set<String> localAddresses = StreamEx.of(VpnIpResolverImpl.getLocalAddresses())
                .map(InetAddress::getHostAddress)
                .toSet();

        log.info("===== ANALYZING CURRENT ROUTES =====");
        Map<Boolean, List<Route>> grouped = StreamEx.of(existingRoutes)
                .mapToEntry(r -> isRouteDeletable(r, vpnIpAddress, localAddresses))
                .invert()
                .grouping();

        log.info("Routes to be skipped:");
        List<Route> keepRoutes = grouped.getOrDefault(Boolean.FALSE, Collections.emptyList());
        keepRoutes.forEach(r -> log.info("> {}", r));

        List<Route> removeRoutes = grouped.getOrDefault(Boolean.TRUE, Collections.emptyList());
        if (removeRoutes.isEmpty()) {
            log.info("No routes to be deleted.");
        } else {
            log.info("Routes to be deleted:");
            removeRoutes.forEach(r -> log.info("> {}", r));
        }

        // Определяем, остались ли у нас все необходимые роуты?
        List<Route> addRoutes = getRequiredRoutes(keepRoutes, removeRoutes, vpnIpAddress);

        return RouteFilterResult.builder()
                .removeRoutes(removeRoutes)
                .keepRoutes(keepRoutes)
                .addRoutes(addRoutes)
                .build();
    }

    private static List<Route> getRequiredRoutes(Collection<Route> keepRoutes, Collection<Route> removeRoutes, String vpnIpAddress) {
        List<RouteExclusion> absentRoutes = StreamEx.of(REAL_BANK_ROUTES)
                .mapToEntry(re -> StreamEx.of(keepRoutes)
                        .anyMatch(re::matches))
                .filterValues(m -> !m)
                .keys()
                .toList();
        if (absentRoutes.isEmpty()) {
            log.info("All required routes are present now.");
            return Collections.emptyList();
        }

        log.info("Routes that should be added:");
        String mostCommonGateway = StreamEx.of(removeRoutes)
                .mapToEntry(Route::getGateway, Function.identity())
                .sortedBy(Map.Entry::getKey)
                .collapseKeys()
                .mapValues(List::size)
                .maxBy(Map.Entry::getValue)
                .map(Map.Entry::getKey)
                .orElseThrow();
        return StreamEx.of(absentRoutes)
                .map(ar -> new Route(ar.getAddress(),
                        ar.getMask(),
                        mostCommonGateway,
                        vpnIpAddress,
                        CREATE_ROUTE_METRIC))
                .peek(r -> log.info("> {}", r))
                .toList();
    }

    private static boolean isRouteDeletable(Route route, String vpnIpAddress, Collection<String> localAddresses) {
        // Роуты на локальную машину.
        if (localAddresses.contains(route.getAddress())) {
            return false;
        }

        // Если шлюз = NO_GATEWAY, то не стоит удалять этот маршрут.
        // Хотя нет, если L2TP, то удалять нужно.
        if (Route.NO_GATEWAY.equals(route.getGateway())) {
            // return false;
        }

        // Пропускаем все адреса, которые не роутятся через интерфейс VPN-подключения.
        if (!vpnIpAddress.equals(route.getNetworkInterface())) {
            return false;
        }

        // Все маршруты, кроме тех, которые указаны в исключениях, подлежат удалению.
        return REAL_BANK_ROUTES.stream()
                .noneMatch(re -> re.matches(route));
    }

    @Value
    private static class RouteExclusion {

        String address;
        String mask;

        public boolean matches(Route route) {
            return route.getAddress().equals(address)
                   && route.getMask().equals(mask);
        }
    }
}
