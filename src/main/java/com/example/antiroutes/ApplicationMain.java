package com.example.antiroutes;

import com.example.antiroutes.api.Route;
import com.example.antiroutes.api.RouteChanger;
import com.example.antiroutes.api.RouteEnumerator;
import com.example.antiroutes.api.RouteFilter;
import com.example.antiroutes.api.RouteFilter.RouteFilterResult;
import com.example.antiroutes.api.VpnIpResolver;
import com.example.antiroutes.config.ApplicationConfig;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.Banner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;

import java.util.List;

@SpringBootApplication
@Import(ApplicationConfig.class)
@RequiredArgsConstructor
@Slf4j
public class ApplicationMain implements CommandLineRunner {

    private final VpnIpResolver vpnIpResolver;
    private final RouteEnumerator routeEnumerator;
    private final RouteFilter routeFilter;
    private final RouteChanger routeChanger;

    @Override
    public void run(String... args) {
        try {
            hackTheRoutes();

            log.info("Done.");
        } catch (Exception ex) {
            log.error("Exception raised!: {}", ex.getMessage());
        }
    }

    private void hackTheRoutes() {
        // Поиск IP подключения к внутренней сети.
        String vpnIpAddress = vpnIpResolver.resolveVpnIpAddress();
        log.info("Detected VPN IP: {}", vpnIpAddress);

        // Получение списка текущих роутов.
        List<Route> routes = routeEnumerator.enumerateRoutes();
        log.info("Found totally {} routes.", routes.size());

        // Определение желаемых действий над роутами.
        RouteFilterResult filterResult = routeFilter.splitRoutes(routes, vpnIpAddress);

        log.info("===== EXECUTING CHANGES =====");
        List<Route> routesToDelete = filterResult.getRemoveRoutes();
        List<Route> routesToCreate = filterResult.getAddRoutes();
        routesToDelete.forEach(routeChanger::killRoute);
        routesToCreate.forEach(routeChanger::createRoute);
    }

    public static void main(String[] args) {
        //noinspection EmptyTryBlock
        try (var ignored = new SpringApplicationBuilder()
                .bannerMode(Banner.Mode.OFF)
                .sources(ApplicationMain.class)
                .build()
                .run(args)) {
        }
    }
}
