package com.example.antiroutes.api;

public interface RouteChanger {

    void createRoute(Route route);

    void killRoute(Route route);
}
