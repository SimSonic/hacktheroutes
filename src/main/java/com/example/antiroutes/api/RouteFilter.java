package com.example.antiroutes.api;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface RouteFilter {

    RouteFilterResult splitRoutes(List<Route> existingRoutes, String vpnIpAddress);

    @Value
    @Builder
    class RouteFilterResult {

        List<Route> removeRoutes;
        List<Route> keepRoutes;
        List<Route> addRoutes;
    }
}
