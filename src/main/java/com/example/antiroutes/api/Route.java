package com.example.antiroutes.api;

import lombok.Value;

@Value
public class Route {

    public static final String NO_GATEWAY = "Available directly";

    /**
     * Адрес назначения.
     */
    String address;

    /**
     * Маска подсети адреса назначения.
     */
    String mask;

    /**
     * Шлюз. IPv4 или константа NO_GATEWAY.
     */
    String gateway;

    /**
     * Сетевой интерфейс, через который доступен целевой адрес или его шлюз.
     */
    String networkInterface;

    /**
     * Метрика.
     */
    int metric;
}
