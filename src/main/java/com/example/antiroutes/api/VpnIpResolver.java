package com.example.antiroutes.api;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface VpnIpResolver {

    String VPN_IP_SUBNET_PREFIX = "10.40.";

    String resolveVpnIpAddress();
}
