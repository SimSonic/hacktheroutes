package com.example.antiroutes.api;

import java.util.List;

@SuppressWarnings("InterfaceMayBeAnnotatedFunctional")
public interface RouteEnumerator {

    List<Route> enumerateRoutes();
}
