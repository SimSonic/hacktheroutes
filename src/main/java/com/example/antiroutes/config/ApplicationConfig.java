package com.example.antiroutes.config;

import com.example.antiroutes.api.RouteEnumerator;
import com.example.antiroutes.api.RouteFilter;
import com.example.antiroutes.api.RouteChanger;
import com.example.antiroutes.api.VpnIpResolver;
import com.example.antiroutes.impl.RouteFilterImpl;
import com.example.antiroutes.impl.VpnIpResolverImpl;
import com.example.antiroutes.impl.windows.RouteEnumeratorWindowsImpl;
import com.example.antiroutes.impl.windows.RouteChangerWindowsImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
public class ApplicationConfig {

    @Bean
    public OS currentOS() {
        return detectOS();
    }

    @Bean
    public VpnIpResolver vpnIpResolver() {
        return new VpnIpResolverImpl();
    }

    @Bean
    public RouteEnumerator routeEnumerator(OS currentOS) {
        switch (currentOS) {
            case WINDOWS:
                return new RouteEnumeratorWindowsImpl();
            case LINUX:
            case MACOS:
                throw unsupportedOsException(currentOS);
            default:
                throw new IllegalArgumentException("");
        }
    }

    @Bean
    public RouteFilter routeFilter() {
        return new RouteFilterImpl();
    }

    @Bean
    public RouteChanger routeKiller(OS currentOS) {
        switch (currentOS) {
            case WINDOWS:
                return new RouteChangerWindowsImpl();
            case LINUX:
            case MACOS:
                throw unsupportedOsException(currentOS);
            default:
                throw new IllegalArgumentException("");
        }
    }


    private static OS detectOS() {
        String osName = System.getProperty("os.name")
                .toLowerCase();

        if (osName.contains("win")) {
            return OS.WINDOWS;
        }
        if (osName.contains("linux")) {
            return OS.LINUX;
        }
        if (osName.contains("mac")) {
            return OS.MACOS;
        }

        throw new IllegalStateException("Cannot understand which OS is: " + osName);
    }

    private static UnsupportedOperationException unsupportedOsException(OS currentOS) {
        return new UnsupportedOperationException(
                "This operation system is still unsupported: "
                + currentOS);
    }

    enum OS {

        WINDOWS,
        LINUX,
        MACOS,
        ;
    }
}
