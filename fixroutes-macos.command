#!/bin/sh
GWIP=$(netstat -rn | grep 10.40 | awk -F ' ' '{print $2}' | head -n 1)
echo $GWIP
netstat -rn | grep $GWIP  > ./vpnlog

while IFS=" " read -r net gw flag netif; do
    if [ $gw == $GWIP ]; then
	sudo route -n delete "$net" "$gw"
    fi
done < ./vpnlog
sudo route -n add 10.0.0.0 "$GWIP"
#rm ./vpnlog
